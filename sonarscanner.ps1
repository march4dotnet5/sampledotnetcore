Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction'] = 'Stop'

& dotnet new tool-manifest
& dotnet tool install --global dotnet-sonarscanner
#& nuget restore -NoCache  # restore Nuget dependencies
if ([string]::IsNullOrEmpty("$env:CI_MERGE_REQUEST_IID"))
{
    & echo "Running Branch analysis"
    & dotnet sonarscanner begin `
          /o:"march4dotnet5" `
          /k:"march4dotnet5_sampledotnetcore" `
          /d:sonar.login="3ab7c4ff3f9db3b72de848d90c5ef3097e1c0ebf" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.branch.name="$env:CI_COMMIT_BRANCH"
}
else
{
    & echo "Running SQ Merge Request analysis"
    & dotnet sonarscanner begin `
          /o:"march4dotnet5" `
          /k:"march4dotnet5_sampledotnetcore" `
          /d:sonar.login="3ab7c4ff3f9db3b72de848d90c5ef3097e1c0ebf" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.pullrequest.key="$env:CI_MERGE_REQUEST_IID" `
          /d:sonar.pullrequest.branch="$env:CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" `
          /d:sonar.pullrequest.base="$env:CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
}

& "$env:MSBUILD_PATH" `
    .\sampledotcore\sampledotcore.csproj # build the project
& dotnet sonarscanner end `
    /d:sonar.login="3ab7c4ff3f9db3b72de848d90c5ef3097e1c0ebf"
